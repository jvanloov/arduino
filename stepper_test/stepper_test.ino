#include <Stepper.h>


// Show the sequence of pulses. 60 at speed one will mean 1 step/sec
//#define STEPS 15
// Actual number of steps per revolution
#define STEPS 2040

// create an instance of the stepper class, specifying
// the number of steps of the motor and the pins it's
// attached to
Stepper stepper1(STEPS, 11, 13, 10, 12);
Stepper stepper2(STEPS, 4, 6, 5, 7);

// 1 revolution at the "real" number of steps
int stepsToTrigger = 2040;
// 1-9
int rpm = 8;

void setup()
{
  stepper1.setSpeed(rpm);
  stepper2.setSpeed(rpm);
}


void loop()
{
  for (int s=0; s<stepsToTrigger; s++) {
    stepper1.step(1);
    stepper2.step(-1);
  }
  delay(2000);
}
