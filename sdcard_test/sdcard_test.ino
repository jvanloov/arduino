#include <FatReader.h>
#include <SdReader.h>
#include <avr/pgmspace.h>

SdReader card;
FatVolume vol;
FatReader root;
FatReader f;

void setup() {
  Serial.begin(9600);
  Serial.println("Initializing SD card");
  if (!card.init()) {
    Serial.println("Card init. failed!");
    sdErrorCheck();
    while(1);
  }

  // enable optimize read
  card.partialBlockRead(true);

  // fat partition?
  uint8_t part;
  for (part = 0; part < 5; part++) {
    if (vol.init(card, part))
      break;
  }
  if (part == 5) {
    Serial.println("No valid FAT partition!");
    sdErrorCheck();
    while(1);
  }

  // show infos
  Serial.print("Using partition ");
  Serial.print(part, DEC);
  Serial.print(", type is FAT");
  Serial.println(vol.fatType(),DEC);

  if (!root.openRoot(vol)) {
    Serial.println("Can't open root dir!");
    while(1);
  }
  root.
}

void loop() {
}

void sdErrorCheck(void) {
  if (!card.errorCode()) return;
  Serial.print("\n\rSD I/O error: ");
  Serial.print(card.errorCode(), HEX);
  Serial.print(", ");
  Serial.println(card.errorData(), HEX);
  while(1);
}
