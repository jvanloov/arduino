#include <Servo.h>

Servo s;

void move_controlled(int newPosition, int step_delay) {
  int currentPosition = s.read();
  int difference = newPosition - currentPosition;
  int direction = (difference>=0)?1:-1;

  for (int i=0; i<abs(difference); i++) {
    delay(step_delay);
    s.write(currentPosition+(direction*i));    
  }
  delay(step_delay);
  s.write(newPosition);
} 


void setup() {
    s.attach(3);
}

int down = 65;
int up = 90;

void loop() {
  move_controlled(down, 10);
  
  delay(1500);

  move_controlled(up, 50);

  delay(1500);
}
