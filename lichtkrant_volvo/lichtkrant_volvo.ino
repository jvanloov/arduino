#include <avr/pgmspace.h>

#include <Time.h>  
#include <ledmatrix.h>

#define TIME_HEADER  "T"   // Header tag for serial time sync message

LEDMatrix ledMatrix;

boolean scrolled = false;

// How many seconds drift is there per interval. Positive values indicate a
// fast clock (clock time runs ahead of real time), negative values
// indicate a slow clock (clock time runs slower than real time).
const int DRIFT_INTERVAL= 2800;
const int SECONDS_DRIFT_PER_INTERVAL = 3;
int previousAdjust = 0;


const char quote_1[] PROGMEM = "Ideas are like children; there are none so wonderful as your own.";
const char quote_2[] PROGMEM = "It takes more than good memory to have good memories.";
const char quote_3[] PROGMEM = "Your everlasting patience will be rewarded sooner or later.";
const char quote_4[] PROGMEM = "Make two grins grow where there was only a grouch before.";
const char quote_5[] PROGMEM = "As the purse is emptied the heart is filled.";
const char quote_6[] PROGMEM = "Be mischievous and you will not be lonesome.";
const char quote_7[] PROGMEM = "Pray for what you want, but work for the things you need.";
const char quote_8[] PROGMEM = "Good luck is the result of good planning.";
const char quote_9[] PROGMEM = "Smiling often can make you look and feel younger.";
const char quote_10[] PROGMEM = "A friend is a present you give yourself.";
const char quote_11[] PROGMEM = "A single kind word will keep one warm for years.";
const char quote_12[] PROGMEM = "Anger begins with folly, and ends with regret.";
const char quote_13[] PROGMEM = "He who laughs at himself never runs out of things to laugh at.";
const char quote_14[] PROGMEM = "Let there be magic in your smile and firmness in your handshake.";
const char quote_15[] PROGMEM = "If you want the rainbow, you must put up with the rain. D. Parton";
const char quote_16[] PROGMEM = "Nature, time and patience are the three best physicians.";
const char quote_17[] PROGMEM = "Strong and bitter words indicate a weak cause.";
const char quote_18[] PROGMEM = "The beginning of wisdom is to desire it.";
const char quote_19[] PROGMEM = "A friend asks only for your time, not your money.";

const char quote_20[] PROGMEM = "You learn from your mistakes... You will learn a lot today.";
const char quote_21[] PROGMEM = "Be on the lookout for coming events; they cast their shadows beforehand.";
const char quote_22[] PROGMEM = "You must try, or hate yourself for not trying.";
const char quote_23[] PROGMEM = "Whenever possible, keep it simple.";

const char* const quotes[] PROGMEM = { quote_1, quote_2, quote_3, quote_4, quote_5, quote_6,
    quote_7, quote_8, quote_9, quote_10, quote_11, quote_12, quote_13, quote_14, quote_15,
    quote_16, quote_17, quote_18, quote_19, quote_20, quote_21, quote_22, quote_23 };
int QUOTES = 23;

void setup() {
  Serial.begin(9600);
  randomSeed(analogRead(0));
  ledMatrix.prepareLEDMatrix();
  previousAdjust = now();
}


void loop() {
  // Set the time if a message is available on serial IN.
  if (Serial.available()) {
    processSyncMessage();
  }
  
  // Read the time
  int hrs = hour();
  int mins = minute();
  int sec = second();

  // If this Arduino exhibits drift, adjust the time. This is done
  // per interval, by adding or subtracting a number of seconds.
  int currentTime = now();
  if ((currentTime - previousAdjust) > DRIFT_INTERVAL) {
    previousAdjust = currentTime;
    adjustTime(-SECONDS_DRIFT_PER_INTERVAL);
  }
  
  // Vanity stuff - scroll a text once every 15 minutes
  if ((mins % 15 == 0) && !scrolled) {
    long randNumber = random(QUOTES);

    char quote[100];
    strcpy_P(quote,(char*)pgm_read_word(&(quotes[randNumber])));        
    ledMatrix.prepareText(quote + '\0');
    ledMatrix.showTextScrollLeft(30);
    scrolled = true;
  } else if (!(mins % 15 == 0)) {
    scrolled = false;
  }

  // Convert the time to a string that can be displayed on the 
  // LED matrix. We flash the ":" to indicate seconds.
  char txt[6];
  txt[0] = '0' + (hrs/10);
  txt[1] = '0' + (hrs%10);
  if (sec % 2 == 0) {
    txt[2] = ':';
  } else {
    txt[2] = '`';
  }
  txt[3] = '0' + (mins/10);
  txt[4] = '0' + (mins%10);
  txt[5] = '\0';
  ledMatrix.prepareText(txt);
  ledMatrix.showTextStatic(2,10);

}

void processSyncMessage() {
  unsigned long pctime;
  const unsigned long BASE_TIME = 1420070400;

  if(Serial.find(TIME_HEADER)) {
    unsigned long hhmm = Serial.parseInt();
    unsigned long hh = (hhmm/100)*3600;
    unsigned long mm = (hhmm%100)*60;
    pctime = BASE_TIME + hh + mm;
    setTime(pctime);
  }
}


