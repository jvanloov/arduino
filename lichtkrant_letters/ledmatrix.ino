#ifndef _ledmatrix_cpp
#define _ledmatrix_cpp

#include "Arduino.h"
#include "letters.h"

int latchPin = 10;
int clockPin = 13;
int latchPinPORTB = latchPin - 8;
int dataPin = 11;
int clock = 9;
int Reset = 8;

#define ROWS 6
#define COLS 24
byte displayBuffer[ROWS][COLS];

#define TEXTLENGTH_MAX 100
byte text[TEXTLENGTH_MAX][8];
byte textLength;

void copyDigit(byte digit[8], int pos) {
  for (int i=0; i<8; i++) {
    text[pos][i] = digit[i];
  }
}

int setDigit(int digit, int pos) {
  if (digit == 0) { copyDigit(n0,pos); }
  if (digit == 1) { copyDigit(n1,pos); }
  if (digit == 2) { copyDigit(n2,pos); }
  if (digit == 3) { copyDigit(n3,pos); }
  if (digit == 4) { copyDigit(n4,pos); }
  if (digit == 5) { copyDigit(n5,pos); }
  if (digit == 6) { copyDigit(n6,pos); }
  if (digit == 7) { copyDigit(n7,pos); }
  if (digit == 8) { copyDigit(n8,pos); }
  if (digit == 9) { copyDigit(n9,pos); }
}

void setChar(char c, int pos) {
  if (c == 'a') { copyDigit(al,pos); }
  if (c == 'b') { copyDigit(bl,pos); }
  if (c == 'c') { copyDigit(cl,pos); }
  if (c == 'd') { copyDigit(dl,pos); }
  if (c == 'e') { copyDigit(el,pos); }
  if (c == 'f') { copyDigit(fl,pos); }
  if (c == 'g') { copyDigit(gl,pos); }
  if (c == 'h') { copyDigit(hl,pos); }
  if (c == 'i') { copyDigit(il,pos); }
  if (c == 'j') { copyDigit(jl,pos); }
  if (c == 'k') { copyDigit(kl,pos); }
  if (c == 'l') { copyDigit(ll,pos); }
  if (c == 'm') { copyDigit(ml,pos); }
  if (c == 'n') { copyDigit(nl,pos); }
  if (c == 'o') { copyDigit(ol,pos); }
  if (c == 'p') { copyDigit(pl,pos); }
  if (c == 'q') { copyDigit(ql,pos); }
  if (c == 'r') { copyDigit(rl,pos); }
  if (c == 's') { copyDigit(sl,pos); }
  if (c == 't') { copyDigit(tl,pos); }
  if (c == 'u') { copyDigit(ul,pos); }
  if (c == 'v') { copyDigit(vl,pos); }
  if (c == 'w') { copyDigit(wl,pos); }
  if (c == 'x') { copyDigit(xl,pos); }
  if (c == 'y') { copyDigit(yl,pos); }
  if (c == 'z') { copyDigit(zl,pos); }
  if (c == '0') { copyDigit(n0,pos); }
  if (c == '1') { copyDigit(n1,pos); }
  if (c == '2') { copyDigit(n2,pos); }
  if (c == '3') { copyDigit(n3,pos); }
  if (c == '4') { copyDigit(n4,pos); }
  if (c == '5') { copyDigit(n5,pos); }
  if (c == '6') { copyDigit(n6,pos); }
  if (c == '7') { copyDigit(n7,pos); }
  if (c == '8') { copyDigit(n8,pos); }
  if (c == '9') { copyDigit(n9,pos); }
  if (c == ' ') { copyDigit(space,pos); }
  if (c == ':') { copyDigit(colon,pos); }
  if (c == '!') { copyDigit(exclamation,pos); }
}

void prepareText(char str[]) {
  for (int i=0;i<TEXTLENGTH_MAX;i++) {
    setChar(' ',i);
  }
  textLength = 0;
  for (int i=0;str[i]!='\0';i++) {
    setChar(str[i],i);
    textLength = i;
  }
  textLength += 1;
}

void showTextScrollLeft(int duration) {
  int totalPixels = 0;
  for (int i=0;i<textLength;i++) {
    totalPixels += text[i][6]+text[i][7];
  }
  int start = 24;
  int end = -totalPixels;
  for (int i=start;i>end;i--) {
    showTextStatic(i, duration);
  }
}

void showTextScrollRight(int duration) {
  int totalPixels = 0;
  for (int i=0;i<textLength;i++) {
    totalPixels += text[i][6]+text[i][7];
  }
  int start = -totalPixels;
  int end = totalPixels*2;
  for (int i=start;i<end;i++) {
    showTextStatic(i, duration);
  }
}

void showTextStatic(int offset, int duration) {
   clearBuffer();
   int currentPos = offset;
   for (int i=0;i<textLength;i++) {
     byte b = B10000000;
     for (int j=0;j<text[i][6];j++) {
       for (int row=0;row<ROWS;row++) {
         if ((currentPos >= 0) && (currentPos < 24)) {
           if ((text[i][row] & b) > 0) {
             displayBuffer[row][currentPos] = 1;
           }
         }
       }
       b = b >> 1;
       currentPos += 1;
     }
     currentPos += text[i][7];
   }
   for (int i=0;i<duration;i++) {
     sendBuffer(displayBuffer, 800);
   }
}



void showStatic(byte matrix[][24], int height, int duration) {
  copyToBuffer(matrix, height, 0);
  for (int i=0; i<duration; i++) {
    sendBuffer(displayBuffer,800);
  }
}

void showStaticInverted(byte matrix[][24], int height, int duration) {
  copyToBufferInverted(matrix, height, 0);
  for (int i=0; i<duration; i++) {
    sendBuffer(displayBuffer,800);
  }
}

void showFlashing(byte matrix[][24], int height, int times) {
  for (int t=0; t<times; t++) {
    copyToBuffer(matrix, height, 0);
    for (int i=0; i<100; i++) {
      sendBuffer(displayBuffer,800);
    }
    clearBuffer();
    for (int i=0; i<100; i++) {
      sendBuffer(displayBuffer,800);
    }
  }
}

void scrollVertical(byte matrix[][24], int height) {
  int cnt = -5;
  for (int i=0; i<height+5; i++) {
    cnt = cnt + 1;
    copyToBuffer(matrix, height, cnt);
    for (int i=0; i<40; i++) {
      sendBuffer(displayBuffer,800);
    }
  }
}


void clearBuffer() {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 0;
    }
  }
}

void copyToBuffer(byte canvas[][24], int height, int rowIndex) {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 0;
    }
  }
  for (int y=0; y<ROWS; y++) {
    for (int x=0; x<COLS; x++) {
      if (((y+rowIndex) >= 0) &&
          ((y+rowIndex) < height)) {
        displayBuffer[y][x] = canvas[y+rowIndex][x];
      }
    }
  }
  
}

void copyToBufferInverted(byte canvas[][24], int height, int rowIndex) {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 1;
    }
  }
  for (int y=0; y<ROWS; y++) {
    for (int x=0; x<COLS; x++) {
      if (((y+rowIndex) >= 0) &&
          ((y+rowIndex) < height)) {
        displayBuffer[y][x] = 1 - canvas[y+rowIndex][x];
      }
    }
  }
}


/*************************************
 * Low-level matrix controller logic *
 *************************************/

void setupSPI(){
  byte clr;
  SPCR |= ( (1<<SPE) | (1<<MSTR) ); // enable SPI as master
  SPCR &= ~( (1<<SPR1) | (1<<SPR0) ); // clear prescaler bits
  clr=SPSR; // clear SPI status reg
  clr=SPDR; // clear SPI data reg
  SPSR |= (1<<SPI2X); // set prescaler bits
  delay(10);
}

byte spi_transfer(byte data)
{
  SPDR = data;			  // Start the transmission
  while (!(SPSR & (1<<SPIF)))     // Wait the end of the transmission
  {
  };
  return SPDR;			  // return the received byte, we don't need that
}

void latchOn(){
  bitSet(PORTB,latchPinPORTB);
}

void latchOff(){
  bitClear(PORTB,latchPinPORTB);
}

void sendBuffer(byte displayBuffer[6][24], int refreshSpeed) {
  digitalWrite(Reset,HIGH);  // reset 4017 to first row
  digitalWrite(Reset,LOW);
  for (int r=0; r<ROWS; r++) {
    latchOff();  // 74HC595s in serial receive mode
    byte b = 0;
    for (int c=0; c<COLS; c++) {
      if (displayBuffer[r][c] == 1) {
        b |= (0x01 << (7-(c % 8)));
      }
      if ((c > 0) && (((c+1) % 8) == 0)) {
        spi_transfer(b);
        b = 0;
      }
    }
    latchOn();  // 74HC595s in parallel output mode
    delayMicroseconds(refreshSpeed);//waiting a bit
    latchOff();
    for (int c=0; c<(COLS/8); c++) {
      spi_transfer(0);// clearing the data
    }
    latchOn();
    digitalWrite(clock,HIGH); // advance 4017 one row
    digitalWrite(clock,LOW);
  }
}

void prepareLEDMatrix() {
    pinMode(dataPin,OUTPUT);
  pinMode(clockPin,OUTPUT);
  pinMode(latchPin,OUTPUT);
  pinMode(clock,OUTPUT);
  pinMode(Reset,OUTPUT);
  digitalWrite(Reset,HIGH);
  digitalWrite(Reset,LOW);
  setupSPI();
}


#endif /* _ledmatrix_cpp */

