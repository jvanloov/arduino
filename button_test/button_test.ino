#include <Arduino.h>

const int testPin = 7;
const int ledPin  = 13;

void setup() {
  pinMode(testPin, INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
}

int ledState = 0;

void loop() {
  int buttonPressed = digitalRead(testPin);
  
  if (buttonPressed == 0) {
    ledState = 1 - ledState;
  }
  digitalWrite(ledPin, ledState);
  delay(200);
}
