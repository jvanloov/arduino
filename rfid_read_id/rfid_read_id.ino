#include <SoftwareSerial.h>
#include <RFIDreader.h>
#include <avr/pgmspace.h>

RFIDreader reader;
char RFIDtag[14];

void setup() {
  reader.initializeReader();
}


void loop() {
  if (reader.readCard(RFIDtag)) {
    Serial.print("tag id: ");
    Serial.println(RFIDtag);
  }

  delay(2500);
}


