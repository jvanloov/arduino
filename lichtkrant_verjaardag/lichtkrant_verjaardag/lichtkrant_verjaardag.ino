int latchPin = 10;
int clockPin = 13;
int latchPinPORTB = latchPin - 8;
int dataPin = 11;
int clock = 9;
int Reset = 8;

#define ROWS 6
#define COLS 24
byte displayBuffer[ROWS][COLS];
    

byte birthdayCake[8][24] = 
   { { 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0 },
     { 0,0,0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0 },
     { 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0 },
     { 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0 },
     { 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0 },
     { 0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0 },
     { 0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0 },
     { 0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0 }
 };
     
byte bravo[6][24] = 
   { { 1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1 },
     { 1,0,0,1,0,1,0,1,1,0,1,1,0,0,1,0,1,0,0,1,1,0,0,1 },
     { 1,1,1,0,0,1,1,0,0,0,0,0,1,0,1,0,1,0,1,0,0,1,0,1 },
     { 1,0,0,1,0,1,0,0,0,0,1,1,1,0,1,0,1,0,1,0,0,1,0,1 },
     { 1,0,0,1,0,1,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0 },
     { 1,1,1,0,0,1,0,0,0,0,1,1,1,0,0,1,0,0,0,1,1,0,0,1 }  
 };


void setup() {
  Serial.begin(9600);
  pinMode(dataPin,OUTPUT);
  pinMode(clockPin,OUTPUT);
  pinMode(latchPin,OUTPUT);
  pinMode(clock,OUTPUT);
  pinMode(Reset,OUTPUT);
  digitalWrite(Reset,HIGH);
  digitalWrite(Reset,LOW);
  setupSPI();
}


void loop() {
  scrollVertical(birthdayCake, 8);
  delay(1500);
  showStatic(bravo, 6, 600);
  delay(500);
}


void showStatic(byte matrix[][24], int height, int duration) {
  copyToBuffer(matrix, height, 0);
  for (int i=0; i<duration; i++) {
    sendBuffer(displayBuffer,800);
  }
}

void showStaticInverted(byte matrix[][24], int height, int duration) {
  copyToBufferInverted(matrix, height, 0);
  for (int i=0; i<duration; i++) {
    sendBuffer(displayBuffer,800);
  }
}

void showFlashing(byte matrix[][24], int height, int times) {
  for (int t=0; t<times; t++) {
    copyToBuffer(matrix, height, 0);
    for (int i=0; i<100; i++) {
      sendBuffer(displayBuffer,800);
    }
    clearBuffer();
    for (int i=0; i<100; i++) {
      sendBuffer(displayBuffer,800);
    }
  }
}

void scrollVertical(byte matrix[][24], int height) {
  int cnt = -5;
  for (int i=0; i<height+5; i++) {
    cnt = cnt + 1;
    copyToBuffer(matrix, height, cnt);
    for (int i=0; i<40; i++) {
      sendBuffer(displayBuffer,800);
    }
  }
}


void clearBuffer() {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 0;
    }
  }
}

void copyToBuffer(byte canvas[][24], int height, int rowIndex) {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 0;
    }
  }
  for (int y=0; y<ROWS; y++) {
    for (int x=0; x<COLS; x++) {
      if (((y+rowIndex) >= 0) &&
          ((y+rowIndex) < height)) {
        displayBuffer[y][x] = canvas[y+rowIndex][x];
      }
    }
  }
  
}

void copyToBufferInverted(byte canvas[][24], int height, int rowIndex) {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 1;
    }
  }
  for (int y=0; y<ROWS; y++) {
    for (int x=0; x<COLS; x++) {
      if (((y+rowIndex) >= 0) &&
          ((y+rowIndex) < height)) {
        displayBuffer[y][x] = 1 - canvas[y+rowIndex][x];
      }
    }
  }
}

void sendBuffer(byte displayBuffer[6][24], int refreshSpeed) {
  digitalWrite(Reset,HIGH);  // reset 4017 to first row
  digitalWrite(Reset,LOW);
  for (int r=0; r<ROWS; r++) {
    latchOff();  // 74HC595s in serial receive mode
    byte b = 0;
    for (int c=0; c<COLS; c++) {
      if (displayBuffer[r][c] == 1) {
        b |= (0x01 << (7-(c % 8)));
      }
      if ((c > 0) && (((c+1) % 8) == 0)) {
        spi_transfer(b);
        b = 0;
      }
    }
    latchOn();  // 74HC595s in parallel output mode
    delayMicroseconds(refreshSpeed);//waiting a bit
    latchOff();
    for (int c=0; c<(COLS/8); c++) {
      spi_transfer(0);// clearing the data
    }
    latchOn();
    digitalWrite(clock,HIGH); // advance 4017 one row
    digitalWrite(clock,LOW);
  }
}


void setupSPI(){
  byte clr;
  SPCR |= ( (1<<SPE) | (1<<MSTR) ); // enable SPI as master
  SPCR &= ~( (1<<SPR1) | (1<<SPR0) ); // clear prescaler bits
  clr=SPSR; // clear SPI status reg
  clr=SPDR; // clear SPI data reg
  SPSR |= (1<<SPI2X); // set prescaler bits
  delay(10);
}

byte spi_transfer(byte data)
{
  SPDR = data;			  // Start the transmission
  while (!(SPSR & (1<<SPIF)))     // Wait the end of the transmission
  {
  };
  return SPDR;			  // return the received byte, we don't need that
}

void latchOn(){
  bitSet(PORTB,latchPinPORTB);
}

void latchOff(){
  bitClear(PORTB,latchPinPORTB);
}

