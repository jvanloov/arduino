
#include <ledmatrix.h>

LEDMatrix ledMatrix;

byte whitespaceLogo[16][24] = 
   { { 0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
     { 0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0 },
     { 0,0,1,0,0,0,0,0,1,0,0,0,1,0,1,1,0,0,0,0,0,0,0,0 },
     { 0,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,1,1,0,0,0,0 },
     { 0,0,1,0,0,0,1,0,1,1,0,0,1,0,1,0,0,1,0,0,1,0,0,0 },
     { 0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,1,1,1,1,0,0,0 },
     { 0,0,1,1,1,1,1,0,1,0,1,0,1,0,1,0,0,1,0,0,0,0,0,0 },
     { 0,0,0,1,0,1,0,0,1,0,1,0,1,0,0,1,0,0,1,1,1,1,1,1 },
     { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }, 
     { 0,0,0,1,1,1,0,1,1,0,0,1,1,0,0,0,1,0,0,0,1,0,0,0 }, 
     { 0,0,1,0,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,0 }, 
     { 0,0,1,1,1,1,0,1,0,1,0,1,1,1,0,1,0,0,0,1,1,0,0,0 }, 
     { 0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0 }, 
     { 1,1,1,1,1,0,0,1,1,0,0,0,1,1,0,0,1,0,0,0,1,1,1,0 }, 
     { 0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0 }, 
     { 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }  
 };
     

byte newlineLogo[6][24] = 
   { { 1,0,0,1,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0 },
     { 1,1,0,1,0,0,1,0,0,1,0,1,0,1,0,0,0,0,1,0,0,0,1,0 },
     { 1,0,1,1,0,1,0,1,0,1,1,1,0,1,0,1,0,1,0,1,0,1,0,1 },
     { 1,0,0,1,0,1,1,1,0,1,1,1,0,1,0,1,0,1,0,1,0,1,1,1 },
     { 1,0,0,1,0,1,0,0,0,1,1,1,0,1,0,1,0,1,0,1,0,1,0,0 },
     { 1,0,0,1,0,0,1,1,0,0,1,0,0,1,0,1,0,1,0,1,0,0,1,1 }  
 };
   

void setup() {
  Serial.begin(9600);
  ledMatrix.prepareLEDMatrix();
}


void loop() {
  ledMatrix.randomize(60);
  delay(1000);
  ledMatrix.showStatic(newlineLogo, 6, 600);
  ledMatrix.prepareText("-by-");
  ledMatrix.showTextStatic(3,250);
  delay(500);
  ledMatrix.scrollVertical(whitespaceLogo, 16);
  delay(500);

  ledMatrix.prepareText("3, 4, 5 april 2015");
  ledMatrix.showTextScrollLeft(20);
  ledMatrix.prepareText("@ whitespace hq");
  ledMatrix.showTextScrollLeft(20);
}





