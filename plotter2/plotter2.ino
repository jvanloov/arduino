#include <Stepper.h>
#include <Servo.h>

// Actual number of steps per revolution
#define STEPS 2040

// create an instance of the stepper class, specifying
// the number of steps of the motor and the pins it's
// attached to
Stepper stepper1(STEPS, 11, 13, 10, 12);  // Y - bridge
Stepper stepper2(STEPS, 4, 6, 5, 7);      // X - pen
Servo s;

//int MAX_STEPS_X = 11000;
//int MAX_STEPS_Y = 10200;

// for testing
int MAX_STEPS_X = 9999;
int MAX_STEPS_Y = 9999;

// Stepper movement direction vs. board coordinate system:
// positive movement requires setting negative stepper direction
int STEPPER_FACTOR_X = -1;
int STEPPER_FACTOR_Y = -1;

int rpm = 8;

int cur_X = 0;
int cur_Y = 0;

boolean millimeters = false;

//double POINTS_PER_MM = 60.606;
double POINTS_PER_MM = 63;


void move_home() {
  move_to(0,0);
}

void set_home_position() {
  Serial.println("Move to home position");
  int home_reached = 0;
  while (!home_reached) {
    int buf[512];
    int i = 0;
    buf[0] = 0;
    while (Serial.available()) {
      buf[i++] = Serial.read();
    }  
    buf[i]=0;
    
    if ((buf[0] == 'w') || (buf[0] == 'W')) {
      stepper2.step(1);
    } else if ((buf[0] == 'x') || (buf[0] == 'X')) {
      stepper2.step(10);
    } else if ((buf[0] == 'c') || (buf[0] == 'C')) {
      stepper2.step(100);
    } else if ((buf[0] == 'q') || (buf[0] == 'Q')) {
      stepper1.step(1);
    } else if ((buf[0] == 's') || (buf[0] == 'S')) {
      stepper1.step(10);
    } else if ((buf[0] == 'd') || (buf[0] == 'D')) {
      stepper1.step(100);
    } else if (buf[0] =='h') {
      home_reached = 1;
      cur_X = 0;
      cur_Y = 0;
    }

    delay(250);
  }
  Serial.println("Home position set");
}


void move_to(int new_X, int new_Y) {
  int move_X = new_X - cur_X;
  int move_Y = new_Y - cur_Y;

  // never go outside board limits: if the current position +
  // the amount to move is outside the board, stop at the edge
  if (cur_X + move_X > MAX_STEPS_X) {
    move_X = MAX_STEPS_X - cur_X;
  }
  if (cur_Y + move_Y > MAX_STEPS_Y) {
    move_Y = MAX_STEPS_Y - cur_Y;
  }
  
  int direction_X= (move_X>0)?1:-1;
  int direction_Y= (move_Y>0)?1:-1;
  
  if (abs(move_X) == abs(move_Y)) {
    for (int x=0; x<abs(move_X); x++) {
      stepper1.step(direction_Y*STEPPER_FACTOR_Y);
      cur_Y += direction_Y;
      stepper2.step(direction_X*STEPPER_FACTOR_X);
      cur_X += direction_X;
    }
  } else if (abs(move_X) > abs(move_Y)) {
    // every <x> steps on the X axis, move 1 step on the Y axis
    double step_size = ((double)abs(move_X)) / abs(move_Y);
    //Serial.print("step size = ");
    //Serial.println(step_size);
    
    int y=0;
    for (int x=0; x<abs(move_X); x++) {
      stepper2.step(direction_X*STEPPER_FACTOR_X);
      cur_X += direction_X;
      if ((x > 0) && (cur_Y != new_Y) && ((x / step_size) > y)) {
        stepper1.step(direction_Y*STEPPER_FACTOR_Y);
        cur_Y += direction_Y;
        y++;
      }
    } 
  } else {
      // every <x> steps on the Y axis, move 1 step on the X axis
      double step_size = ((double)abs(move_Y)) / abs(move_X);
      //Serial.print("step size = ");
      //Serial.println(step_size);
      
      int x=0;
      for (int y=0; y<abs(move_Y); y++) {
        stepper1.step(direction_Y*STEPPER_FACTOR_Y);
        cur_Y += direction_Y;
        if ((y > 0) && (cur_X != new_X) && ((y / step_size) > x)) {
          stepper2.step(direction_X*STEPPER_FACTOR_X);
          cur_X += direction_X;
          x++;
        }
      }
  }  
  
  //Serial.print("cur_X = ");
  //Serial.println(cur_X);
  //Serial.print("cur_Y = ");
  //Serial.println(cur_Y);
} 


void move_controlled(int newPosition, int step_delay) {
  int currentPosition = s.read();
  int difference = newPosition - currentPosition;
  int direction = (difference>=0)?1:-1;

  for (int i=0; i<abs(difference); i++) {
    delay(step_delay);
    s.write(currentPosition+(direction*i));    
  }
  delay(step_delay);
  s.write(newPosition);
} 

void set_pen(bool pen_up) {
  if (pen_up) {
    move_controlled(90, 10);
  } else {
    move_controlled(65, 10);
  }
} 

// Circle algorithm from http://slabode.exofire.net/circle_draw.shtml
void circle(int centerX, int centerY, int radius, int num_segments) {
  double theta = 2 * 3.1415926 / (double)num_segments; 
  double c = cosf(theta); //precalculate the sine and cosine
  double s = sinf(theta);
  double t;

  double x = radius;  //we start at angle = 0 
  double y = 0; 

  move_to((int)(x + centerX), centerY);  //output vertex 
  set_pen(false);
  for(int ii = 0; ii < num_segments+1; ii++) 
  { 
    int new_X = (int)x + centerX;
    int new_Y = (int)y + centerY;
    move_to(new_X, new_Y);  //output vertex 
        
    //apply the rotation matrix
    t = x;
    x = c * x - s * y;
    y = s * t + c * y;
  } 
  set_pen(true);

}

void line(int topX, int topY, int botX, int botY) {
  move_to(topX, topY);
  set_pen(false);
  move_to(botX, botY);
  set_pen(true);    
}

void square(int topX, int topY, int botX, int botY) {
  if (topX > botX) {
    int tempX = botX;
    botX = topX;
    topX = tempX;
  }
  if (topY > botY) {
    int tempY = botY;
    botY = topY;
    topY = tempY;
  }
  move_to(topX, topY);
  set_pen(false);
  move_to(botX, topY);
  move_to(botX, botY);
  move_to(topX, botY);
  move_to(topX, topY);
  set_pen(true);  
}


void setup() {
  Serial.begin(9600);

  stepper1.setSpeed(rpm);
  stepper2.setSpeed(rpm);
  s.attach(3);
  set_pen(true);

  set_home_position();
}

void loop() {
  // Read what's in the communications buffer. We assume single commands are sent, and new 
  // commands are sent when after confirmation of execution was received.
  int buf[512];
  int i = 0;
  while (Serial.available()) {
    buf[i++] = Serial.read();
  }  
  buf[i]=0;

  // Interpret the command.
  // Single-letter commands: "u", "d" = pen up/down; "m", "p" = coordinates are in mm or points
  // 8 letter commands are move commands: two four-character coordinates (0000-9999)
  // 17-letter commands are compound commands: "s", "l", "c" = square/line/circle followed by 4 four-character coordinates
  if (i==2) {
    if ((buf[0] == 'u') || (buf[0] == 'U')) {
      set_pen(true);
      Serial.println("OK");
    } else if ((buf[0] == 'd') || (buf[0] == 'D')) {
      set_pen(false);
      Serial.println("OK");
    } else if ((buf[0] == 'm') || (buf[0] == 'M')) {
      millimeters = true;
      Serial.println("MM mode set");
    } else if ((buf[0] == 'p') || (buf[0] == 'P')) {
      millimeters = false;
      Serial.println("PP mode set");
    }
  } else if (i == 9) {
    int newX = (buf[0]-48)*1000 + (buf[1]-48)*100 + (buf[2]-48)*10 + (buf[3]-48);
    int newY = (buf[4]-48)*1000 + (buf[5]-48)*100 + (buf[6]-48)*10 + (buf[7]-48);

    // if values are in millimeters, convert to points first
    if (millimeters) {
      newX = round(newX * POINTS_PER_MM);
      newY = round(newY * POINTS_PER_MM);
    }
  
    move_to(newX,newY);
   
    stepper1.motorOff();
    stepper2.motorOff();
    Serial.println("OK");
  } else if (i == 18) {
    int topX = (buf[1]-48)*1000 + (buf[2]-48)*100 + (buf[3]-48)*10 + (buf[4]-48);
    int topY = (buf[5]-48)*1000 + (buf[6]-48)*100 + (buf[7]-48)*10 + (buf[8]-48);
    int botX = (buf[9]-48)*1000 + (buf[10]-48)*100 + (buf[11]-48)*10 + (buf[12]-48);
    int botY = (buf[13]-48)*1000 + (buf[14]-48)*100 + (buf[15]-48)*10 + (buf[16]-48);

    // if values are in millimeters, convert to points first
    if (millimeters) {
      topX = round(topX * POINTS_PER_MM);
      topY = round(topY * POINTS_PER_MM);
      botX = round(botX * POINTS_PER_MM);
      botY = round(botY * POINTS_PER_MM);
    }

    if ((buf[0] == 's') || (buf[0] == 'S')) {
      square(topX,topY,botX,botY);
    }
    if ((buf[0] == 'l') || (buf[0] == 'L')) {
      line(topX,topY,botX,botY);
    }
    if ((buf[0] == 'c') || (buf[0] == 'C')) {
      circle(topX,topY,botX,20);
    }
    
    stepper1.motorOff();
    stepper2.motorOff();
    Serial.println("OK");
  }
  delay(25);  

}
