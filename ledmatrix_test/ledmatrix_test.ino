
#include <ledmatrix.h>

LEDMatrix ledMatrix;

byte bitmapExample[6][24] = 
   { { 0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0 },
     { 0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0 },
     { 0,1,1,0,0,0,0,0,0,1,0,1,1,0,1,0,0,0,0,0,0,1,1,0 },
     { 1,0,0,1,0,0,1,0,0,1,0,1,1,0,1,0,0,1,0,0,1,0,0,1 },
     { 0,0,0,0,1,1,0,0,0,1,0,0,0,0,1,0,0,0,1,1,0,0,0,0 },
     { 0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0 } };
   

void setup() {
  Serial.begin(9600);
  ledMatrix.prepareLEDMatrix();
}


void loop() {
//  ledMatrix.randomize(60);
//  delay(1000);
  ledMatrix.showStatic(bitmapExample, 6, 600);
  delay(1000);
//  ledMatrix.scrollVertical(bitmapExample, 6);
//  delay(1000);

  ledMatrix.prepareText("Proficiat met je 2e verjaardag, O-J!");
  ledMatrix.showTextScrollLeft(15);
  delay(1000);
}





