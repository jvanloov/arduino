#include <Stepper.h>
#include <Servo.h>

// Actual number of steps per revolution
#define STEPS 2040

// create an instance of the stepper class, specifying
// the number of steps of the motor and the pins it's
// attached to
Stepper stepper1(STEPS, 11, 13, 10, 12);  // X
Stepper stepper2(STEPS, 4, 6, 5, 7);      // Y

Servo s;

// 1-9
int rpm = 8;

int current_direction_X = 1;
int current_direction_Y = 1;

int line_position = 0;

#define DIRECT_MODE 0
#define LETTER_MODE 1
int mode = DIRECT_MODE; 

#define STEP_SIZE 10
#define MAX_LINE_LENGTH 75

#define CHAR_WIDTH 15

void setup()
{
  Serial.begin(9600);

  stepper1.setSpeed(rpm);
  stepper2.setSpeed(rpm);
  s.attach(3);
  set_pen(true);
}

void set_pen(bool pen_up) {
  delay(100);
  if (pen_up) {
    s.write(75);
  } else {
    s.write(50);
  }
  delay(100);
} 

void change_direction_X(int direction) {
  current_direction_X = direction;
  stepper1.step(2 * current_direction_X);
  Serial.print('C');
}

void change_direction_Y(int direction) {
  current_direction_Y = direction;
  stepper2.step(2 * current_direction_Y);
  Serial.print('C');
}

void step_X(int direction) {
    if (current_direction_X != direction) {
      change_direction_X(direction);
    }
    stepper1.step(CHAR_WIDTH * direction);
}

void step_Y(int direction) {
    if (current_direction_Y != direction) {
      change_direction_Y(direction);
    }
    stepper2.step(CHAR_WIDTH * direction);
    if (mode == LETTER_MODE) {
      line_position += -direction;
      //Serial.println(line_position);
    }
}

void step_diagonal(int directionX, int directionY) {
    if (current_direction_X != directionX) {
      change_direction_X(directionX);
    }
    if (current_direction_Y != directionY) {
      change_direction_Y(directionY);
    }
    for (int i=0; i<CHAR_WIDTH; i++) {
      stepper1.step(directionX);
      stepper2.step(directionY);
    }
    if (mode == LETTER_MODE) {
      line_position += -directionY;
      //Serial.println(line_position);
    }
}


void interpret_direct_char(int ch) {
    if (ch == 'e') {
      step_X(-STEP_SIZE);
    } else if (ch == 'x') {
      step_X(STEP_SIZE);
    } else if (ch == 's') {
      step_Y(-STEP_SIZE);
    } else if (ch == 'd') {
      step_Y(STEP_SIZE);
    } else if (ch == 'z') {
      step_diagonal(-STEP_SIZE,-STEP_SIZE);
    } else if (ch == 'r') {
      step_diagonal(-STEP_SIZE,STEP_SIZE);
    } else if (ch == 'w') {
      step_diagonal(STEP_SIZE,-STEP_SIZE);
    } else if (ch == 'c') {
      step_diagonal(STEP_SIZE,STEP_SIZE);
    } else if (ch == 'a') {
      set_pen(true);
    } else if (ch == 'q') {
      set_pen(false);
    }
}

#define LETTERS 27
char* letters[LETTERS][2] = {
  { "a", "qxxxxwzeeeeaxxqddaeesss" },
  { "b", "qxxxxxszrdasqzerdasss" },
  { "c", "ssxqrcxxxwzaeeees" },
  { "d", "qxxxxxszeeerdasss" },
  { "e", "qxxxxxssaddeeqsadeeeqssas" },
  { "f", "qxxxxxssaddeeqsaeeess" },
  { "g", "sxxqsercxxxwzaeeees" },
  { "h", "qxxxxxaeeqssaxxqeeeeeas" },
  { "i", "qxxxxxaeeeees" },
  { "j", "xqzwxxxxaeeeees" },
  { "k", "qxxxxxaeeqwwarrqzzeas" },
  { "l", "xxxxxqeeeeessas" },
  { "m", "qxxxxxzweeeeeas" },
  { "n", "qxxxxxzzxxaeeqeeeas" },
  { "o", "sqcxxxwzeeerass" },
  { "p", "qxxxxxszrdaeeesss" },
  { "q", "ssqxxxxxdrzsaeees" },
  { "r", "qxxxxxszrdzzeas" },
  { "s", "qswccxwsaeeeees" },
  { "t", "sqxxxxxdasqsaeeeees" },
  { "u", "xxxxxqeeeezswxxxxaeeeees" },
  { "v", "xxxxxqeeeeewwxxxaeeeees" },
  { "w", "xxxxxqeeeeewzxxxxxaeeeees" },
  { "x", "qxxwswxadddqezszeeas" },
  { "y", "qswxdcxxassqeeeaees" }, 
  { "z", "sss qdddxwwwxdddaeeeeessss" },
  { " ", "sss"}
};

void carriage_return() {
  Serial.println('<');
  while (line_position > 0) {
    step_Y(STEP_SIZE);
    delay(15);
  }
  for (int i=0; i<6; i++) {
    step_X(-STEP_SIZE);
    delay(15);
  }
}

void interpret_letter_char(int ch) {
  if (ch == '<') {  // "carriage return"
    carriage_return();
  } else {
    if ((line_position+STEP_SIZE) > (MAX_LINE_LENGTH*STEP_SIZE)) {
      carriage_return();
    }
    char *commands;
    for (int i=0; i<LETTERS; i++) {
      if (ch == letters[i][0][0]) {
        commands = letters[i][1];
        break;
      }
    }
    for (int i=0; i<strlen(commands); i++) {
      interpret_direct_char(commands[i]);
      delay(15);
    }
  }
}


void loop()
{
  int buf[512];
  int i = 0;
  while (Serial.available()) {
    buf[i++] = Serial.read();
  }  

  for (int j=0; j<i; j++) {
    int ch = buf[j];
    Serial.print((char)ch);
    if (ch == '#') {
      mode = DIRECT_MODE;
    } else if (ch == '@') {
      mode = LETTER_MODE;
      line_position = 0;
      Serial.print("<");
    }
    
    if (mode == DIRECT_MODE) {
      interpret_direct_char(ch);
    } else if (mode == LETTER_MODE) {
      interpret_letter_char(ch);
    }
  }
}
