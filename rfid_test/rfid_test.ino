#include <SoftwareSerial.h>

//  rfid settings
SoftwareSerial RFIDSerial(8, 6);

int RFIDindex = 0;
int RFIDResetPin = 7;
char RFIDtag[14];
boolean RFIDreading = false;

void setup() {
  Serial.begin(9600);
  Serial.println("debug");
  Serial.println("----------------");

  // rfid setup
  pinMode(RFIDResetPin, OUTPUT);
  resetReader();
  RFIDSerial.begin(9600);

  Serial.println("> rfid ready");
}

void loop() {
  RFIDindex = 0;

  //  rfid data?
  while(RFIDSerial.available()) {
    int readByte = RFIDSerial.read();
    Serial.print("byte: ");
    Serial.write(readByte);
    Serial.println();

    if(readByte == 2) RFIDreading = true;
    if(readByte == 3) RFIDreading = false;

    if(RFIDreading && readByte != 2 && readByte != 10 && readByte != 13){
      RFIDtag[RFIDindex] = readByte;
      RFIDindex++;
    }
  }
  
  if (strlen(RFIDtag) > 0) {
    Serial.println("rfid tag id: ");
    for(int d = 0; d < 12; d++) {
      Serial.print(RFIDtag[d]);
    }
    Serial.println();
  } else {
    Serial.println("no tag detected");
  }
  
  delay(1000);
  clearTag(RFIDtag);
  resetReader();
}

void resetReader() {
  digitalWrite(RFIDResetPin, LOW);
  digitalWrite(RFIDResetPin, HIGH);
  delay(150);
}

void clearTag(char one[]) {
  for(int i = 0; i < strlen(one); i++){
    one[i] = 0;
  }
}

