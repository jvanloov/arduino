#include <Stepper.h>
#include <Servo.h>

// Show the sequence of pulses. 60 at speed one will mean 1 step/sec
//#define STEPS 15
// Actual number of steps per revolution
#define STEPS 2040

// create an instance of the stepper class, specifying
// the number of steps of the motor and the pins it's
// attached to
Stepper stepper1(STEPS, 11, 13, 10, 12);  // X
Stepper stepper2(STEPS, 4, 6, 5, 7);      // Y

Servo s;

int stepsToTriggerX = 50;
int stepsToTriggerY = 40;
int stepsPerDash = 10;

// 1-9
int rpm = 8;

int pen_up = 80;
int pen_down = 50;

void setup()
{
  Serial.begin(9600);
  randomSeed(analogRead(0));

  stepper1.setSpeed(rpm);
  stepper2.setSpeed(rpm);
  s.attach(3);
}

void set_pen(bool pen_is_down) {
  if (pen_is_down) {
    s.write(pen_down);
  } else {
    s.write(pen_up);
  }
} 


void loop()
{
  int stepCounter = 0;
  bool pen_is_down = false;
  
  for (int s=0; s<stepsToTriggerX; s++, stepCounter++) {
    stepper1.step(30);
    if (stepCounter % stepsPerDash == 0) {
      pen_is_down = !pen_is_down;
      set_pen(pen_is_down);
      Serial.print(pen_is_down);
      Serial.print('\n');
    }
  }

  for (int s=0; s<stepsToTriggerY; s++, stepCounter++) {
    stepper2.step(30);
    if (stepCounter % stepsPerDash == 0) {
      pen_is_down = !pen_is_down;
      set_pen(pen_is_down);
      Serial.print(pen_is_down);
      Serial.print('\n');
    }
  }
  
  for (int s=0; s<stepsToTriggerX; s++, stepCounter++) {
    stepper1.step(-30);
    if (stepCounter % stepsPerDash == 0) {
      pen_is_down = !pen_is_down;
      set_pen(pen_is_down);
      Serial.print(pen_is_down);
      Serial.print('\n');
    }
  }

  for (int s=0; s<stepsToTriggerY; s++, stepCounter++) {
    stepper2.step(-30);
    if (stepCounter % stepsPerDash == 0) {
      pen_is_down = !pen_is_down;
      set_pen(pen_is_down);
      Serial.print(pen_is_down);
      Serial.print('\n');
    }
  }

  delay(2000);
}
