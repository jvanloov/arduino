#ifndef RFIDreader_h
#define RFIDreader_h

#include "Arduino.h"
#include <SoftwareSerial.h>

class RFIDreader {
 
  public:

  RFIDreader();
  void initializeReader();
  bool readCard(char tag[]);

  private: 
  
  SoftwareSerial RFIDSerial;
  int RFIDResetPin;

  void resetReader();
  void clearTag(char one[]);

};

#endif