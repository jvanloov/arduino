#include "Arduino.h"
#include "RFIDreader.h"

RFIDreader::RFIDreader() : RFIDSerial(8,6)
 {
  RFIDResetPin = 7;
}

void RFIDreader::initializeReader() {
  Serial.begin(9600);
  pinMode(RFIDResetPin, OUTPUT);
  digitalWrite(RFIDResetPin, HIGH);
  RFIDSerial.begin(9600);
  Serial.println("> rfid ready");
}
  
bool RFIDreader::readCard(char tag[]) {
  bool RFIDreading = false;
  int RFIDindex = 0;

  clearTag(tag);
  while (RFIDSerial.available()) {
    int readByte = RFIDSerial.read();

    if(readByte == 2) RFIDreading = true;
    if(readByte == 3) RFIDreading = false;

    if(RFIDreading && readByte != 2 && readByte != 10 && readByte != 13){
      tag[RFIDindex] = readByte;
      RFIDindex++;
    }
  }
  
  if (RFIDindex > 0) {
    resetReader();
    return true;
  } else {
    return false;
  }
}

void RFIDreader::resetReader() {
  digitalWrite(RFIDResetPin, LOW);
  digitalWrite(RFIDResetPin, HIGH);
  delay(150);
}

void RFIDreader::clearTag(char one[]) {
  for(int i = 0; i < strlen(one); i++){
    one[i] = 0;
  }
}
