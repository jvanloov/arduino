#include "Arduino.h"
#include "ledmatrix.h"
#include "letters.h"

void LEDMatrix::copyDigit(byte digit[6], int pos) {
  for (int i=0; i<6; i++) {
    text[pos][i] = digit[i];
  }
}

void LEDMatrix::setDigit(int digit, int pos) {
  if (digit == 0) { copyDigit(n0,pos); }
  if (digit == 1) { copyDigit(n1,pos); }
  if (digit == 2) { copyDigit(n2,pos); }
  if (digit == 3) { copyDigit(n3,pos); }
  if (digit == 4) { copyDigit(n4,pos); }
  if (digit == 5) { copyDigit(n5,pos); }
  if (digit == 6) { copyDigit(n6,pos); }
  if (digit == 7) { copyDigit(n7,pos); }
  if (digit == 8) { copyDigit(n8,pos); }
  if (digit == 9) { copyDigit(n9,pos); }
}

void LEDMatrix::setChar(char c, int pos) {
  if (c == 'a') { copyDigit(al,pos); }
  if (c == 'b') { copyDigit(bl,pos); }
  if (c == 'c') { copyDigit(cl,pos); }
  if (c == 'd') { copyDigit(dl,pos); }
  if (c == 'e') { copyDigit(el,pos); }
  if (c == 'f') { copyDigit(fl,pos); }
  if (c == 'g') { copyDigit(gl,pos); }
  if (c == 'h') { copyDigit(hl,pos); }
  if (c == 'i') { copyDigit(il,pos); }
  if (c == 'j') { copyDigit(jl,pos); }
  if (c == 'k') { copyDigit(kl,pos); }
  if (c == 'l') { copyDigit(ll,pos); }
  if (c == 'm') { copyDigit(ml,pos); }
  if (c == 'n') { copyDigit(nl,pos); }
  if (c == 'o') { copyDigit(ol,pos); }
  if (c == 'p') { copyDigit(pl,pos); }
  if (c == 'q') { copyDigit(ql,pos); }
  if (c == 'r') { copyDigit(rl,pos); }
  if (c == 's') { copyDigit(sl,pos); }
  if (c == 't') { copyDigit(tl,pos); }
  if (c == 'u') { copyDigit(ul,pos); }
  if (c == 'v') { copyDigit(vl,pos); }
  if (c == 'w') { copyDigit(wl,pos); }
  if (c == 'x') { copyDigit(xl,pos); }
  if (c == 'y') { copyDigit(yl,pos); }
  if (c == 'z') { copyDigit(zl,pos); }
  if (c == 'A') { copyDigit(au,pos); }
  if (c == 'B') { copyDigit(bu,pos); }
  if (c == 'C') { copyDigit(cu,pos); }
  if (c == 'D') { copyDigit(du,pos); }
  if (c == 'E') { copyDigit(eu,pos); }
  if (c == 'F') { copyDigit(fu,pos); }
  if (c == 'G') { copyDigit(gu,pos); }
  if (c == 'H') { copyDigit(hu,pos); }
  if (c == 'I') { copyDigit(iu,pos); } 
  if (c == 'J') { copyDigit(ju,pos); }
  if (c == 'K') { copyDigit(ku,pos); }
  if (c == 'L') { copyDigit(lu,pos); }
  if (c == 'M') { copyDigit(mu,pos); }
  if (c == 'N') { copyDigit(nu,pos); }
  if (c == 'O') { copyDigit(ou,pos); }
  if (c == 'P') { copyDigit(pu,pos); }
  if (c == 'Q') { copyDigit(qu,pos); }
  if (c == 'R') { copyDigit(ru,pos); }
  if (c == 'S') { copyDigit(su,pos); }
  if (c == 'T') { copyDigit(tu,pos); }
  if (c == 'U') { copyDigit(uu,pos); }
  if (c == 'V') { copyDigit(vu,pos); }
  if (c == 'W') { copyDigit(wu,pos); }
  if (c == 'X') { copyDigit(xu,pos); }
  if (c == 'Y') { copyDigit(yu,pos); }
  if (c == 'Z') { copyDigit(zu,pos); }
  if (c == '0') { copyDigit(n0,pos); }
  if (c == '1') { copyDigit(n1,pos); }
  if (c == '2') { copyDigit(n2,pos); }
  if (c == '3') { copyDigit(n3,pos); }
  if (c == '4') { copyDigit(n4,pos); }
  if (c == '5') { copyDigit(n5,pos); }
  if (c == '6') { copyDigit(n6,pos); }
  if (c == '7') { copyDigit(n7,pos); }
  if (c == '8') { copyDigit(n8,pos); }
  if (c == '9') { copyDigit(n9,pos); }
  if (c == ' ') { copyDigit(space,pos); }
  if (c == '`') { copyDigit(space_small,pos); }
  if (c == '.') { copyDigit(dot,pos); }
  if (c == ',') { copyDigit(comma,pos); }
  if (c == ':') { copyDigit(colon,pos); }
  if (c == ';') { copyDigit(semicolon,pos); }
  if (c == '!') { copyDigit(exclamation,pos); }
  if (c == '-') { copyDigit(dash,pos); }
  if (c == '@') { copyDigit(at,pos); }
  if (c == '\'') { copyDigit(quote,pos); }
}

void LEDMatrix::prepareText(char str[]) {
  for (int i=0;i<TEXTLENGTH_MAX;i++) {
    setChar(' ',i);
  }
  textLength = 0;
  for (int i=0;str[i]!='\0';i++) {
    setChar(str[i],i);
    textLength = i;
  }
  textLength += 1;
}

byte charLength(byte character[6]) {
    byte res = 0;
    for (int i=1; i<6; i++) {
        res = (res << 1) + (character[i] & B00000001);
    }
    return res;
}

void LEDMatrix::showTextScrollLeft(int duration) {
  int totalPixels = 0;
  for (int i=0;i<textLength;i++) {
    totalPixels += charLength(text[i])+1;  // the 1 is the empty column between characters
  }
  int start = 24;
  int end = -totalPixels;
  for (int i=start;i>end;i--) {
    showTextStatic(i, duration);
  }
}

void LEDMatrix::showTextScrollRight(int duration) {
  int totalPixels = 0;
  for (int i=0;i<textLength;i++) {
      totalPixels += charLength(text[i])+1;  // the 1 is the empty column between characters
  }
  int start = -totalPixels;
  int end = totalPixels*2;
  for (int i=start;i<end;i++) {
    showTextStatic(i, duration);
  }
}

void LEDMatrix::showTextStatic(int offset, int duration) {
   clearBuffer();
   int currentPos = offset;
   for (int i=0;i<textLength;i++) {
     byte b = B10000000;
     for (int j=0;j<charLength(text[i]);j++) {
       for (int row=0;row<ROWS;row++) {
         if ((currentPos >= 0) && (currentPos < 24)) {
           if ((text[i][row] & b) > 0) {
             displayBuffer[row][currentPos] = 1;
           }
         }
       }
       b = b >> 1;
       currentPos += 1;
     }
     // Decide whether to add an empty column between characters.
     // If the character explicitly says it needs a following empty column,
     // add one immediately.
     // Otherwise, leave an empty column if this would cause horizontally 
     // or diagonally adjacent pixels to be on.
     // If there are no "touching" pixels, we can "glue" the characters
     // together.
     if ((text[i][0] & B00000001) == B00000001) {
       currentPos += 1;
     } else if (i < textLength) {
       int touches = 0;
       int horTouches = 0;
       for (int row=0;row<ROWS;row++) {
         if ((text[i][row] & (B10000000 >> (charLength(text[i])-1))) > 0) {
           if ((text[i+1][row] & B10000000) > 0) {
             horTouches += 1;
             touches += 1;
           }
           if ((text[i+1][min(ROWS,row+1)] & B10000000) > 0) {
             touches += 1;     
           }
           if ((text[i+1][max(0,row-1)] & B10000000) > 0) {
             touches += 1;     
           }
         }
       }
       if ((horTouches >= 1) || (touches > 1)) {
         currentPos += 1;
       }
     }
   }
   for (int i=0;i<duration;i++) {
     sendBuffer(displayBuffer, DISPLAY_TIME);
   }
}



void LEDMatrix::showStatic(byte matrix[][24], int height, int duration) {
  copyToBuffer(matrix, height, 0);
  for (int i=0; i<duration; i++) {
    sendBuffer(displayBuffer,DISPLAY_TIME);
  }
}

void LEDMatrix::showStaticInverted(byte matrix[][24], int height, int duration) {
  copyToBufferInverted(matrix, height, 0);
  for (int i=0; i<duration; i++) {
    sendBuffer(displayBuffer,DISPLAY_TIME);
  }
}

void LEDMatrix::showFlashing(byte matrix[][24], int height, int times) {
  for (int t=0; t<times; t++) {
    copyToBuffer(matrix, height, 0);
    for (int i=0; i<100; i++) {
      sendBuffer(displayBuffer,DISPLAY_TIME);
    }
    clearBuffer();
    for (int i=0; i<100; i++) {
      sendBuffer(displayBuffer,DISPLAY_TIME);
    }
  }
}

void LEDMatrix::scrollVertical(byte matrix[][24], int height) {
  int cnt = -5;
  for (int i=0; i<height+5; i++) {
    cnt = cnt + 1;
    copyToBuffer(matrix, height, cnt);
    for (int i=0; i<40; i++) {
      sendBuffer(displayBuffer,DISPLAY_TIME);
    }
  }
}


void LEDMatrix::clearBuffer() {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 0;
    }
  }
}

void LEDMatrix::copyToBuffer(byte canvas[][24], int height, int rowIndex) {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 0;
    }
  }
  for (int y=0; y<ROWS; y++) {
    for (int x=0; x<COLS; x++) {
      if (((y+rowIndex) >= 0) &&
          ((y+rowIndex) < height)) {
        displayBuffer[y][x] = canvas[y+rowIndex][x];
      }
    }
  }
  
}

void LEDMatrix::copyToBufferInverted(byte canvas[][24], int height, int rowIndex) {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 1;
    }
  }
  for (int y=0; y<ROWS; y++) {
    for (int x=0; x<COLS; x++) {
      if (((y+rowIndex) >= 0) &&
          ((y+rowIndex) < height)) {
        displayBuffer[y][x] = 1 - canvas[y+rowIndex][x];
      }
    }
  }
}

void LEDMatrix::randomize(int times) {
  for (int i=0; i<times; i++) {
     for (int p=0; p<20; p++) {
       int x = random(24);
       int y = random(6);
       int w = random(2);
       displayBuffer[y][x] = w;
     }
      for (int j=0; j<10; j++) {
        sendBuffer(displayBuffer,DISPLAY_TIME);
      }
  }
  clearBuffer();
}

/*************************************
 * Low-level matrix controller logic *
 *************************************/

void LEDMatrix::setupSPI(){
  byte clr;
  SPCR |= ( (1<<SPE) | (1<<MSTR) ); // enable SPI as master
  SPCR &= ~( (1<<SPR1) | (1<<SPR0) ); // clear prescaler bits
  clr=SPSR; // clear SPI status reg
  clr=SPDR; // clear SPI data reg
  SPSR |= (1<<SPI2X); // set prescaler bits
  delay(10);
}

byte LEDMatrix::spi_transfer(byte data)
{
  SPDR = data;			  // Start the transmission
  while (!(SPSR & (1<<SPIF)))     // Wait the end of the transmission
  {
  };
  return SPDR;			  // return the received byte, we don't need that
}

void LEDMatrix::latchOn(){
  bitSet(PORTB,latchPinPORTB);
}

void LEDMatrix::latchOff(){
  bitClear(PORTB,latchPinPORTB);
}

void LEDMatrix::sendBuffer(byte displayBuffer[6][24], int displayTime) {
  digitalWrite(Reset,HIGH);  // reset 4017 to first row
  digitalWrite(Reset,LOW);
  for (int r=0; r<ROWS; r++) {
    latchOff();  // 74HC595s in serial receive mode
    byte b = 0;
    for (int c=0; c<COLS; c++) {
      if (displayBuffer[r][c] == 1) {
        b |= (0x01 << (7-(c % 8)));
      }
      if ((c > 0) && (((c+1) % 8) == 0)) {
        spi_transfer(b);
        b = 0;
      }
    }
    latchOn();  // 74HC595s in parallel output mode
    delayMicroseconds(displayTime);//waiting a bit
    latchOff();
    for (int c=0; c<(COLS/8); c++) {
      spi_transfer(0);// clearing the data
    }
    latchOn();
    digitalWrite(clock,HIGH); // advance 4017 one row
    digitalWrite(clock,LOW);
  }
}

void LEDMatrix::prepareLEDMatrix() {
    pinMode(dataPin,OUTPUT);
  pinMode(clockPin,OUTPUT);
  pinMode(latchPin,OUTPUT);
  pinMode(clock,OUTPUT);
  pinMode(Reset,OUTPUT);
  digitalWrite(Reset,HIGH);
  digitalWrite(Reset,LOW);
  setupSPI();
}


