#ifndef _ledmatrix_h
#define _ledmatrix_h

#include "Arduino.h"

#define ROWS 6
#define COLS 24
#define TEXTLENGTH_MAX 85
#define DISPLAY_TIME 400

class LEDMatrix {

  public:
    // Initializer
    void prepareLEDMatrix();

    // Text related functions.
    void prepareText(char str[]);
    void showTextScrollLeft(int duration);
    void showTextScrollRight(int duration);
    void showTextStatic(int offset, int duration);

    // Bitmap related functions.
    void showStatic(byte matrix[][24], int height, int duration);
    void showStaticInverted(byte matrix[][24], int height, int duration);
    void showFlashing(byte matrix[][24], int height, int times);
    void scrollVertical(byte matrix[][24], int height);

    void randomize(int times);

  private:
    int latchPin = 10;
    int clockPin = 13;
    int latchPinPORTB = latchPin - 8;
    int dataPin = 11;
    int clock = 9;
    int Reset = 8;

    byte displayBuffer[ROWS][COLS];
    byte text[TEXTLENGTH_MAX][6];
    byte textLength;

    void copyDigit(byte digit[6], int pos);
    void setDigit(int digit, int pos);
    void setChar(char c, int pos);
    void clearBuffer();
    void copyToBuffer(byte canvas[][24], int height, int rowIndex);
    void copyToBufferInverted(byte canvas[][24], int height, int rowIndex);

    void setupSPI();
    byte spi_transfer(byte data);
    void latchOn();
    void latchOff();
    void sendBuffer(byte displayBuffer[6][24], int displayTime);

};

#endif /* _ledmatrix_h */

