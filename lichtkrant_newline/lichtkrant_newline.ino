#include <Time.h>  

int latchPin = 10;
int clockPin = 13;
int latchPinPORTB = latchPin - 8;
int dataPin = 11;
int clock = 9;
int Reset = 8;

#define ROWS 6
#define COLS 24
byte displayBuffer[ROWS][COLS];
    
byte n1[6] = {B00100000,B01100000,B10100000,B00100000,B00100000,B00100000};
byte n2[6] = {B01100000,B10010000,B00010000,B01100000,B10000000,B11110000};
byte n3[6] = {B01100000,B10010000,B00100000,B00010000,B10010000,B01100000};
byte n4[6] = {B00010000,B00110000,B01010000,B11110000,B00010000,B00010000};
byte n5[6] = {B11110000,B10000000,B11100000,B00010000,B10010000,B01100000};
byte n6[6] = {B01100000,B10000000,B11100000,B10010000,B10010000,B01100000};
byte n7[6] = {B11110000,B00010000,B00100000,B00100000,B00100000,B00100000};
byte n8[6] = {B01100000,B10010000,B01100000,B10010000,B10010000,B01100000};
byte n9[6] = {B01100000,B10010000,B01110000,B00010000,B10010000,B01100000};
byte n0[6] = {B01100000,B10010000,B10010000,B10010000,B10010000,B01100000};
byte colon[6] = {B00000000,B00000000,B01100000,B00000000,B01100000,B00000000};
byte space[6] = {B00000000,B00000000,B00000000,B00000000,B00000000,B00000000};

byte dottedPattern[6][24] =
  { { 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0 },
    { 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1 },
    { 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0 },
    { 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1 },
    { 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0 },
    { 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1 } };
    

byte whitespaceLogo[16][24] = 
   { { 0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
     { 0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0 },
     { 0,0,1,0,0,0,0,0,1,0,0,0,1,0,1,1,0,0,0,0,0,0,0,0 },
     { 0,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,1,1,0,0,0,0 },
     { 0,0,1,0,0,0,1,0,1,1,0,0,1,0,1,0,0,1,0,0,1,0,0,0 },
     { 0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,1,1,1,1,0,0,0 },
     { 0,0,1,1,1,1,1,0,1,0,1,0,1,0,1,0,0,1,0,0,0,0,0,0 },
     { 0,0,0,1,0,1,0,0,1,0,1,0,1,0,0,1,0,0,1,1,1,1,1,1 },
     { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }, 
     { 0,0,0,1,1,1,0,1,1,0,0,1,1,0,0,0,1,0,0,0,1,0,0,0 }, 
     { 0,0,1,0,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,0 }, 
     { 0,0,1,1,1,1,0,1,0,1,0,1,1,1,0,1,0,0,0,1,1,0,0,0 }, 
     { 0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0 }, 
     { 1,1,1,1,1,0,0,1,1,0,0,0,1,1,0,0,1,0,0,0,1,1,1,0 }, 
     { 0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0 }, 
     { 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }  
 };
     
byte newlineLogo[6][24] = 
   { { 1,0,0,1,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0 },
     { 1,1,0,1,0,0,1,0,0,1,0,1,0,1,0,0,0,0,1,0,0,0,1,0 },
     { 1,0,1,1,0,1,0,1,0,1,1,1,0,1,0,1,0,1,0,1,0,1,0,1 },
     { 1,0,0,1,0,1,1,1,0,1,1,1,0,1,0,1,0,1,0,1,0,1,1,1 },
     { 1,0,0,1,0,1,0,0,0,1,1,1,0,1,0,1,0,1,0,1,0,1,0,0 },
     { 1,0,0,1,0,0,1,1,0,0,1,0,0,1,0,1,0,1,0,1,0,0,1,1 }  
 };
     
byte byLogo[6][24] = 
   { { 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
     { 0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0 },
     { 0,0,0,0,0,0,0,1,1,1,0,0,1,0,0,1,0,0,0,0,0,0,0,0 },
     { 0,0,0,1,1,1,0,1,0,0,1,0,0,1,1,1,0,1,1,1,0,0,0,0 },
     { 0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0 },
     { 0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0 }  
 };



#define TEXTLENGTH 5
byte text[TEXTLENGTH][6];

#define TIME_HEADER  "T"   // Header tag for serial time sync message

void setup() {
  Serial.begin(9600);
  pinMode(dataPin,OUTPUT);
  pinMode(clockPin,OUTPUT);
  pinMode(latchPin,OUTPUT);
  pinMode(clock,OUTPUT);
  pinMode(Reset,OUTPUT);
  digitalWrite(Reset,HIGH);
  digitalWrite(Reset,LOW);
  setupSPI();
}


void loop() {
  if (Serial.available()) {
    processSyncMessage();
  }

  randomize(60);
  delay(1000);
  showStatic(newlineLogo, 6, 600);
  showFlashing(byLogo, 6, 3);
  delay(500);
  scrollVertical(whitespaceLogo, 16);
  delay(500);

  int hrs = hour();
  int mins = minute();

  setDigit(hrs/10,0);
  setDigit(hrs%10,1);
  copyDigit(colon,2);
  setDigit(mins/10,3);
  setDigit(mins%10,4);
  
  showTextStatic(text,1000);
  delay(500);
}

void processSyncMessage() {
  unsigned long pctime;
  const unsigned long BASE_TIME = 1420070400;

  if(Serial.find(TIME_HEADER)) {
    unsigned long hhmm = Serial.parseInt();
    unsigned long hh = (hhmm/100)*3600;
    unsigned long mm = (hhmm%100)*60;
    pctime = BASE_TIME + hh + mm;
    setTime(pctime);
  }
}

void copyDigit(byte digit[6], int pos) {
  for (int i=0; i<6; i++) {
    text[pos][i] = digit[i];
  }
}

int setDigit(int digit, int pos) {
  if (digit == 0) {
    copyDigit(n0,pos);
  }
  if (digit == 1) {
    copyDigit(n1,pos);
  }
  if (digit == 2) {
    copyDigit(n2,pos);
  }
  if (digit == 3) {
    copyDigit(n3,pos);
  }
  if (digit == 4) {
    copyDigit(n4,pos);
  }
  if (digit == 5) {
    copyDigit(n5,pos);
  }
  if (digit == 6) {
    copyDigit(n6,pos);
  }
  if (digit == 7) {
    copyDigit(n7,pos);
  }
  if (digit == 8) {
    copyDigit(n8,pos);
  }
  if (digit == 9) {
    copyDigit(n9,pos);
  }
}


void showTextStatic(byte text[][6], int duration) {
   clearBuffer();
   for (int i=0;i<TEXTLENGTH;i++) {
     for (int row=0;row<ROWS;row++) {
       byte b = B10000000;
       for (int j=0;j<4;j++) {
         if ((text[i][row] & b) > 0) {
           displayBuffer[row][i*5+j] = 1;
         }
         b = b >> 1;
       }
     }
   }
   for (int i=0;i<duration;i++) {
     sendBuffer(displayBuffer, 800);
   }
}

void randomize(int times) {
  copyToBuffer(dottedPattern,6,0);
  for (int i=0; i<times; i++) {
     for (int p=0; p<20; p++) {
       int x = random(24);
       int y = random(6);
       int w = random(2);
       displayBuffer[y][x] = w;
     }
      for (int j=0; j<10; j++) {
        sendBuffer(displayBuffer,800);
      }
  }
  clearBuffer();
}

void showStatic(byte matrix[][24], int height, int duration) {
  copyToBuffer(matrix, height, 0);
  for (int i=0; i<duration; i++) {
    sendBuffer(displayBuffer,800);
  }
}

void showStaticInverted(byte matrix[][24], int height, int duration) {
  copyToBufferInverted(matrix, height, 0);
  for (int i=0; i<duration; i++) {
    sendBuffer(displayBuffer,800);
  }
}

void showFlashing(byte matrix[][24], int height, int times) {
  for (int t=0; t<times; t++) {
    copyToBuffer(matrix, height, 0);
    for (int i=0; i<100; i++) {
      sendBuffer(displayBuffer,800);
    }
    clearBuffer();
    for (int i=0; i<100; i++) {
      sendBuffer(displayBuffer,800);
    }
  }
}

void scrollVertical(byte matrix[][24], int height) {
  int cnt = -5;
  for (int i=0; i<height+5; i++) {
    cnt = cnt + 1;
    copyToBuffer(matrix, height, cnt);
    for (int i=0; i<40; i++) {
      sendBuffer(displayBuffer,800);
    }
  }
}


void clearBuffer() {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 0;
    }
  }
}

void copyToBuffer(byte canvas[][24], int height, int rowIndex) {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 0;
    }
  }
  for (int y=0; y<ROWS; y++) {
    for (int x=0; x<COLS; x++) {
      if (((y+rowIndex) >= 0) &&
          ((y+rowIndex) < height)) {
        displayBuffer[y][x] = canvas[y+rowIndex][x];
      }
    }
  }
  
}

void copyToBufferInverted(byte canvas[][24], int height, int rowIndex) {
  for (int x=0; x<COLS; x++) {
    for (int y=0; y<ROWS; y++) {
      displayBuffer[y][x] = 1;
    }
  }
  for (int y=0; y<ROWS; y++) {
    for (int x=0; x<COLS; x++) {
      if (((y+rowIndex) >= 0) &&
          ((y+rowIndex) < height)) {
        displayBuffer[y][x] = 1 - canvas[y+rowIndex][x];
      }
    }
  }
}

void sendBuffer(byte displayBuffer[6][24], int refreshSpeed) {
  digitalWrite(Reset,HIGH);  // reset 4017 to first row
  digitalWrite(Reset,LOW);
  for (int r=0; r<ROWS; r++) {
    latchOff();  // 74HC595s in serial receive mode
    byte b = 0;
    for (int c=0; c<COLS; c++) {
      if (displayBuffer[r][c] == 1) {
        b |= (0x01 << (7-(c % 8)));
      }
      if ((c > 0) && (((c+1) % 8) == 0)) {
        spi_transfer(b);
        b = 0;
      }
    }
    latchOn();  // 74HC595s in parallel output mode
    delayMicroseconds(refreshSpeed);//waiting a bit
    latchOff();
    for (int c=0; c<(COLS/8); c++) {
      spi_transfer(0);// clearing the data
    }
    latchOn();
    digitalWrite(clock,HIGH); // advance 4017 one row
    digitalWrite(clock,LOW);
  }
}


void setupSPI(){
  byte clr;
  SPCR |= ( (1<<SPE) | (1<<MSTR) ); // enable SPI as master
  SPCR &= ~( (1<<SPR1) | (1<<SPR0) ); // clear prescaler bits
  clr=SPSR; // clear SPI status reg
  clr=SPDR; // clear SPI data reg
  SPSR |= (1<<SPI2X); // set prescaler bits
  delay(10);
}

byte spi_transfer(byte data)
{
  SPDR = data;			  // Start the transmission
  while (!(SPSR & (1<<SPIF)))     // Wait the end of the transmission
  {
  };
  return SPDR;			  // return the received byte, we don't need that
}

void latchOn(){
  bitSet(PORTB,latchPinPORTB);
}

void latchOff(){
  bitClear(PORTB,latchPinPORTB);
}

