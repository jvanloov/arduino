#include <Time.h>  
#include <ledmatrix.h>

#define TIME_HEADER  "T"   // Header tag for serial time sync message

LEDMatrix ledMatrix;

void setup() {
  Serial.begin(9600);
  ledMatrix.prepareLEDMatrix();
}


void loop() {
  if (Serial.available()) {
    processSyncMessage();
  }
  
  int hrs = hour();
  int mins = minute();

  char txt[5];
  txt[0] = '0' + (hrs/10);
  txt[1] = '0' + (hrs%10);
  txt[2] = ':';
  txt[3] = '0' + (mins/10);
  txt[4] = '0' + (mins%10);
  ledMatrix.prepareText(txt);
  ledMatrix.showTextStatic(2,1000);

  int hr = hrs%12;
  if (mins >= 30) { hr = (hrs+1)%12; }
  if (hr == 0) { hr = 12; }

  String uur;
  if (hr == 1) { uur = "een"; }
  if (hr == 2) { uur = "twee"; }
  if (hr == 3) { uur = "drie"; }
  if (hr == 4) { uur = "vier"; }
  if (hr == 5) { uur = "vijf"; }
  if (hr == 6) { uur = "zes"; }
  if (hr == 7) { uur = "zeven"; }
  if (hr == 8) { uur = "acht"; }
  if (hr == 9) { uur = "negen"; }
  if (hr == 10) { uur = "tien"; }
  if (hr == 11) { uur = "elf"; }
  if (hr == 12) { uur = "twaalf"; }
  
  if (mins == 0) {
    delay(500);
    String msg = uur + " uur";
    char mesg[msg.length()+1];
    msg.toCharArray(mesg, msg.length()+1);
    mesg[0] = mesg[0] - 32;
    ledMatrix.prepareText(mesg);
    ledMatrix.showTextScrollLeft(20);
    delay(500);
  }
  if (mins == 15) {
    delay(500);
    String msg = "Kwart over " + uur;
    char mesg[msg.length()+1]; 
    msg.toCharArray(mesg, msg.length()+1);
    ledMatrix.prepareText(mesg);
    ledMatrix.showTextScrollLeft(20);
    delay(500);
  }
  if (mins == 30) {
    delay(500);
    String msg = "Half " + uur;
    char mesg[msg.length()+1]; 
    msg.toCharArray(mesg, msg.length()+1);
    ledMatrix.prepareText(mesg);
    ledMatrix.showTextScrollLeft(20);
    delay(500);
  }
  if (mins == 45) {
    delay(500);
    String msg = "Kwart voor " + uur;
    char mesg[msg.length()+1]; 
    msg.toCharArray(mesg, msg.length()+1);
    ledMatrix.prepareText(mesg);
    ledMatrix.showTextScrollLeft(20);
    delay(500);
  }
}

void processSyncMessage() {
  unsigned long pctime;
  const unsigned long BASE_TIME = 1420070400;

  if(Serial.find(TIME_HEADER)) {
    unsigned long hhmm = Serial.parseInt();
    unsigned long hh = (hhmm/100)*3600;
    unsigned long mm = (hhmm%100)*60;
    pctime = BASE_TIME + hh + mm;
    setTime(pctime);
  }
}


