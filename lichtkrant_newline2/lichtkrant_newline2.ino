#include <avr/pgmspace.h>

#include <Time.h>  
#include <ledmatrix.h>

#define TIME_HEADER  "T"   // Header tag for serial time sync message

LEDMatrix ledMatrix;

boolean scrolled = false;

// How many seconds drift is there per interval. Positive values indicate a
// fast clock (clock time runs ahead of real time), negative values
// indicate a slow clock (clock time runs slower than real time).
const int DRIFT_INTERVAL= 2800;
const int SECONDS_DRIFT_PER_INTERVAL = 3;
int previousAdjust = 0;


const char quote_1[] PROGMEM = "Newline";
const char quote_2[] PROGMEM = "Coding!!!      Hacking!!!      Arduinos!!!";

const char* const quotes[] PROGMEM = { quote_1, quote_2 };
int QUOTES = 2;

void setup() {
  Serial.begin(9600);
  randomSeed(analogRead(0));
  ledMatrix.prepareLEDMatrix();
  previousAdjust = now();
}


void loop() {
  // Set the time if a message is available on serial IN.
  if (Serial.available()) {
    processSyncMessage();
  }
  
  // Read the time
  int hrs = hour();
  int mins = minute();
  int sec = second();

  // If this Arduino exhibits drift, adjust the time. This is done
  // per interval, by adding or subtracting a number of seconds.
  int currentTime = now();
  if ((currentTime - previousAdjust) > DRIFT_INTERVAL) {
    previousAdjust = currentTime;
    adjustTime(-SECONDS_DRIFT_PER_INTERVAL);
  }
  
  // Vanity stuff - scroll a text once every 15 minutes
  if ((mins % 5 == 0) && !scrolled) {
    long randNumber = random(QUOTES);

    char quote[100];
    strcpy_P(quote,(char*)pgm_read_word(&(quotes[randNumber])));        
    ledMatrix.prepareText(quote + '\0');
    ledMatrix.showTextScrollLeft(18);
    scrolled = true;
  } else if (!(mins % 15 == 0)) {
    scrolled = false;
  }

  // Convert the time to a string that can be displayed on the 
  // LED matrix. We flash the ":" to indicate seconds.
  char txt[6];
  txt[0] = '0' + (hrs/10);
  txt[1] = '0' + (hrs%10);
  if (sec % 2 == 0) {
    txt[2] = ':';
  } else {
    txt[2] = '`';
  }
  txt[3] = '0' + (mins/10);
  txt[4] = '0' + (mins%10);
  txt[5] = '\0';
  ledMatrix.prepareText(txt);
  ledMatrix.showTextStatic(2,10);

}

void processSyncMessage() {
  unsigned long pctime;
  const unsigned long BASE_TIME = 1420070400;

  if(Serial.find(TIME_HEADER)) {
    unsigned long hhmm = Serial.parseInt();
    unsigned long hh = (hhmm/100)*3600;
    unsigned long mm = (hhmm%100)*60;
    pctime = BASE_TIME + hh + mm;
    setTime(pctime);
  }
}


